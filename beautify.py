import json

if __name__ == '__main__':
	with open('verbs.json', encoding='utf-8', mode="r") as verb_file:
		verbs = json.load(verb_file)
		print(json.dumps(verbs, ensure_ascii=False, sort_keys=True, indent=4))