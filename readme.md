`mode`: select which verbs to choose
VALUES:
	`"full"`: all verbs are randomly selected
	`"focus"`: focus/bold verbs which may appear in the written tests
	`"non-focus"`: non-focus verbs

`group`: select which group to choose
VALUES:
	`"any"` (default) Any groups 
	`"1"` Group 1 Verbs ending with the vowel -a, in imperative
	`"2a"` Group 2a Verbs ending with a voiced consonant in imperative
	`"2b"` Group 2b Verbs ending with the unvoiced consonants -k, -p, -s, -t, -x, in imperative
	`"2c"` Group 2c Verbs ending with the consonant -r in imperative have the same form in imperative and in present
	`"3"` Grupp 3 Verbs ending with another vowel than -a in imperative
		NOTE : Group 3 has no focus verbs. It is usable only with `"mode": "full"` or `"mode": "non-focus"`.
	`"4-5"` Group 4-5 Irregular verbs

`random`: random or ordered (when ordered it will have the focus-verb first)

`randomTense`:
VALUES:
	`true` : 1 random tense for 1 verb (useful for learning group patterns)
	`false` : all tenses will be asked (useful for irregular verbs)

`indicateGroup`: When asking a question, show the group of the verb

`indicateFocus`: When asking a question, show if the verb is a focus verb

`moreStatistics`: Print more statistics at the end of the test including:
- groups results
- focus results
- overrall worst tense
- worst tense by verb group