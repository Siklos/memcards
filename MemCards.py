import json
import itertools
import re
from colorama import Fore, Style
from random import randint, shuffle


def verb_generator(verbs, mode, group):
	focus = mode == "focus"
	if mode == "full":
		if group == "any":
			yield from (verb for verb in verbs)
		else:
			yield from (verb for verb in verbs if verb["group"] == group)
	else:
		if group == "any":
			yield from (verb for verb in verbs if verb["focus"] == focus)
		else:
			yield from (verb for verb in verbs if verb["group"] == group and verb["focus"] == focus)


def ask_question(question_number, verb, indicate_group, indicate_focus, correct_answers, wrong_answers):
	verb_word = verb["verb"]
	group = " (Group {})".format(verb["group"]) if indicate_group else ""
	focus = (" (focus verb)" if verb["focus"] else "") if indicate_focus else ""
	if question_number == 5:
		tense = "ENGLISH word"
		verb_word = verb["conjugation"][1]
		answer = verb["verb"]
	else:
		answer = verb["conjugation"][question_number]
		if question_number == 0:
			tense = "IMPERATIVE form"
		elif question_number == 1:
			tense = "INFINITIVE form"
		elif question_number == 2:
			tense = "PRESENT form"
		elif question_number == 3:
			tense = "PRETERIT form"
		elif question_number == 4:
			tense = "SUPINE form"
	question = "What is the {}{}{} form of {}{}{}?{}{}".format(Fore.YELLOW, tense, Style.RESET_ALL, Fore.RED, verb_word, Style.RESET_ALL, group, focus)
	print(question)
	my_answer = ""
	while len(my_answer) == 0:
		my_answer = input()
	if answer == "*":
		if my_answer == "*" or len(my_answer) == 0:
			print(f"{Fore.GREEN}CORRECT!{Style.RESET_ALL}\n")
			correct_answers.append(answer)
		else:
			print(f"{Fore.RED}WRONG!{Style.RESET_ALL}\nAnswer was: \033[1m{Fore.BLUE}{answer}{Style.RESET_ALL}\nYour answer was: {Fore.RED}{my_answer}{Style.RESET_ALL}\n")
			wrong_answers.append((answer, my_answer))
	else:
		if re.search(r'\b' + re.escape(my_answer) + r'\b', answer) is not None:
			print(f"{Fore.GREEN}CORRECT!{Style.RESET_ALL}\n")
			correct_answers.append(answer)
		else:
			print(f"{Fore.RED}WRONG!{Style.RESET_ALL}\nAnswer was: \033[1m{Fore.BLUE}{answer}{Style.RESET_ALL}\nYour answer was: {Fore.RED}{my_answer}{Style.RESET_ALL}\n")
			wrong_answers.append((answer, my_answer))


def main():
	with open("settings.json") as settings_file:
		data = json.load(settings_file)
		mode = data["mode"]
		group = data["group"]
		random = data["random"]
		random_tense = data["randomTense"]
		indicate_group = data["indicateGroup"]
		indicate_focus = data["indicateFocus"]

	correct_answers = []
	wrong_answers = []
	with open('verbs.json', encoding='utf-8', mode="r") as verb_file:
		verbs = json.load(verb_file)
		if random:
			shuffle(verbs)

		for verb in verb_generator(verbs, mode, group):
			if random_tense:
				question_number = randint(0, 5)
				ask_question(question_number, verb, indicate_group, indicate_focus, correct_answers, wrong_answers)
			else:
				for question_number in range(6):
					ask_question(question_number, verb, indicate_group, indicate_focus, correct_answers, wrong_answers)

	print(f"Correct answers: {correct_answers}\n")
	print(f"Wrong answers:")
	if (len(wrong_answers) != 0):
		for (right_answer, wrong_answer) in wrong_answers:
			print(f"{Fore.RED}{wrong_answer}{Style.RESET_ALL} => \033[1m{Fore.BLUE}{right_answer}{Style.RESET_ALL}")
	else:
		print(f"{Fore.GREEN}None! Good job!{Style.RESET_ALL}\n")
	print(f"{len(correct_answers)}/{len(correct_answers) + len(wrong_answers)} corrects answers")


if __name__ == '__main__':
	main()